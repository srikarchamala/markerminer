![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/MarkerMiner_logo_BW_DNA4_V1.jpeg)
** **

**IMPORTANT !!!!** - Use [Chrome](http://www.google.com/chrome/) Web Browser to view this page. As some of the other web browsers were unable to to navigate thourgh web-links.


Using MarkerMiner on iPlant
---
** **

####SECTION 1: Spawn a MarkerMiner instance on the iPlant Atmosphere

The MarkerMiner pipeline and its required dependencies have been encapsulated into a single machine image and are made available via the iPlant Atmosphere infrastructure, allowing users to easily spin up an instance based on this image for their use. Once an instance is started up, it exposes a lightweight web application with a user interface allowing for easy submission of jobs and retrieval of results.

Please follow the step-by-step instructions outlined below to start a new machine instance. For advanced usage information, refer to the official[ Atmosphere Manual](https://pods.iplantcollaborative.org/wiki/x/Iaxm)

 

**1.** Visit[ https://atmo.iplantcollaborative.org](https://atmo.iplantcollaborative.org) (Figure 1.1.) and **Click** *Log in with your iPlant I*D.

![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/image_0.png)

**Figure 1.1.** iPlant Atmosphere login screen

 

**2.** Fill out your iPlant login credentials and **Click** *Login* (Figure 1.2). If you don’t have an account, please visit[ https://user.iplantcollaborative.org](https://user.iplantcollaborative.org) to register for one. First-time users of iPlant Atmosphere need to request access by following the steps outlined here:[ https://pods.iplantcollaborative.org/wiki/x/mIly](https://pods.iplantcollaborative.org/wiki/x/mIly).

![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/image_1.png)

**Figure 1.2.** Login form requesting user to provide iPlant credentials


**3.** On successful login, you will be redirected to your iPlant Atmosphere control panel. On this page, **Click** *Launch New Instance*.

 
**4.** In the *Select an Image* panel (Figure 1.3), **Search** for the "MarkerMiner" image (result should show up under *Other Images*).

![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/image_2.png)

 

**Figure 1.3.** Customizing the machine configuration prior to launching the instance.

 
 4.1.         **Click** on the image name to open the panel on the right

 4.2.         **Enter** a *Name* for your instance

 4.3.         **Choose** an *Instance size* based on your requirements (combination of CPUs and RAM). For more information, refer to[ About Instances and Instance Size](https://pods.iplantcollaborative.org/wiki/x/Blm).
 
 4.4.         **Click** *Launch Instance* to start the instance spawning process

Note: This process can take anywhere up to 30 minutes.


**5.** Use the *My Instances* panel (Figure 1.4) on the left hand side of the Atmosphere control panel to keep track of the status.

![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/image_3.png)

 

![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/image_4.png)

**Figure 1.4.** Progress bar showing the current status during (top) and after (bottom) successful deployment of your personal MarkerMiner machine instance.

 
**6.** On successful activation, you should receive an email message with the following content (Figure 1.5).

![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/image_5.png)

**Figure 1.5.** Email notification on successful instance deployment from iPlant Atmosphere Admin

**7.** In your web browser, type in the Instance **IP Address** (available in the email or on the Atmosphere control panel) into the browser URL bar and hit **Enter**.

**8.** This will display the **MarkerMiner WebApp** online job submission interface. Please refer to the next section for more information or how to configure and run the pipeline, retrieve and examine the results.

**9.** When done with the instance, you may[ terminate](https://pods.iplantcollaborative.org/wiki/display/atmman/Terminating+an+Instance),[ suspend](https://pods.iplantcollaborative.org/wiki/display/atmman/Suspending+and+Resuming+an+Instance), or[ stop](https://pods.iplantcollaborative.org/wiki/display/atmman/Stopping+and+Starting+an+Instance) it. We recommend suspending the instance, as this will place the instance in a state that does not count towards the users’ monthly computational resource quota allocation.

** **

####SECTION 2: Instructions for using the MarkerMiner WebApp

**1. Test Data:**

A test data set is provided to help users familiarize with the MarkerMiner web application.

Click the "Download sample dataset" (Figure 2.1) link to retrieve a copy of sample input FASTA files (and precomputed output files).

 

![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/image_6.png)

**Figure 2.1**: Downloading sample input and output datasets.

**2. Input Data:**

*Data Format and Naming Requirements*—MarkerMiner will accept a path to the directory or folder with assembled transcriptome data files in FASTA format. So all transcriptome data files that needs to analysed need to be place in this folder. Users can process a single FASTA file or multiple FASTA files. However, all file names must use the following naming convention: file names must start with a **four-letter species code followed by a hyphen** (e.g. "DAT1-", "DAT2-", "DAT3-", etc.; illustrated in Figure 2.2). Also, file names should only be ending in either **".fa"** or **".fasta"** or **".fsa"**.

 
*Selecting Input Data*: Click the "Choose File" (or “Browse” depending on your web browser) tab located in the “Input FASTA file(s)” field, select the desired FASTA (.fa) files, and click “Choose”.

![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/image_7.png)

**Figure 2.2**: Selecting transcriptome assembly FASTA files.

 

**3. Adjust Default Parameters:**

Users have the flexibility to adjust any of the default parameters automatically displayed in the MarkerMiner interface (Figure 2.3).

For the steps running BLAST, we are using the default NCBI-BLAST+ parameters outlined in the[ BLAST Command Line Applications User Manual](http://www.ncbi.nlm.nih.gov/books/NBK1763/#_CmdLineAppsManual_Appendix_C_Options_for_) (Camacho et al, 2013).

For example, users have the option to select a closer reference proteome/genome to their group of interest. MarkerMiner’s drop-down menu facilitates currently includes 16 angiosperm references.

Caution: Adjusting default parameters may negatively impact the results of your MarkerMiner run. See the MarkerMiner manuscript (Chamala et al. 2014) for a more detailed discussion of default parameters.

![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/image_8.png)

**Figure 2.3.**: Default parameters are automatically displayed in MarkerMiner.

 

**4. Submit Jobs, Monitor Results, and Retrieve Output:**

Before submitting a MarkerMiner job, users have the option of providing a valid e-mail address that is used to notify the user upon start/completion of the job. Click on the "Submit Job" button to start your run (see Figure 2.3 above).

After a MarkerMiner job is submitted, users are redirected to a new page that displays the job status (e.g. "Job has been submitted successfully") and provides important links to help users monitor a MarkerMiner run (Figure 2.4). For example, users can monitor the current job status via the “browse/view” link or retrieve tar gzipped file output via the “download” link.

Users who did not provide an email address prior to submission should bookmark the status page so that you may return to it to check on the status of the job.

![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/image_9.png)

**Figure 2.4.** Job submission status page containing download links

Users who provided an email address will be sent a job submission notification email (Figure 2.5). Users should save this e-mail, since it includes links that can be used to retrieve their results. Once the job is complete, these users will also receive a final notification email, but it will NOT include a data retrieval link.

![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/image_10.png)

**Figure 2.5.** Job submission e-mail notification from MarkerMiner.

 

![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/image_11.png)

**Figure 2.6.** Job completion e-mail notification from MarkerMiner.

 

**5. Retrieving Results and Output:**

Click on the output retrieval link provided on either the MarkerMiner job submission status page (Figure 2.4) or in the MarkerMiner e-mail notification (Figure 2.5). The output will automatically download as a tar gzipped directory to a users computer (Figure 2.7).

![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/image_12.png)

**Figure 2.7.** MarkerMiner output downloads as a tar gzipped (.tar.gz) directory.

 
