![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/MarkerMiner_logo_BW_DNA4_V1.jpeg)
** **

**IMPORTANT !!!!** - Use [Chrome](http://www.google.com/chrome/) Web Browser to view this page. As some of the other web browsers were unable to to navigate thourgh web-links.


Using MarkerMiner WebApp
---
** **


**1. Sample Dataset:**

A test data set is provided to help users familiarize with the MarkerMiner web application.

Click the "Download sample dataset" (Figure 1.1) link to retrieve a copy of sample input FASTA files (and precomputed output files).

 

![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/image_6.png)

**Figure 1.1**: Downloading sample input and output datasets.

**2. Input Data:**

*Data Format and Naming Requirements*—MarkerMiner will accept a path to the directory or folder with assembled transcriptome data files in FASTA format. So all transcriptome data files that needs to analysed need to be place in this folder. Users can process a single FASTA file or multiple FASTA files. However, all file names must use the following naming convention: file names must start with a **four-letter species code followed by a hyphen** (e.g. "DAT1-", "DAT2-", "DAT3-", etc.; illustrated in Figure 1.2). Also, file names should only be ending in either **".fa"** or **".fasta"** or **".fsa"**.

 

*Selecting Input Data*: Click the "Choose File" (or “Browse” depending on your web browser) tab located in the “Input FASTA file(s)” field, select the desired FASTA (.fa) files, and click “Choose”.

![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/image_7.png)

**Figure 1.2**: Selecting transcriptome assembly FASTA files.

 

**3. Adjust Default Parameters:**

Users have the flexibility to adjust any of the default parameters automatically displayed in the MarkerMiner interface (Figure 1.3).

For the steps running BLAST, we are using the default NCBI-BLAST+ parameters outlined in the[ BLAST Command Line Applications User Manual](http://www.ncbi.nlm.nih.gov/books/NBK1763/#_CmdLineAppsManual_Appendix_C_Options_for_) (Camacho et al, 2013).

For example, users have the option to select a closer reference proteome/genome to their group of interest. MarkerMiner’s drop-down menu facilitates currently includes 16 angiosperm references.

Caution: Adjusting default parameters may negatively impact the results of your MarkerMiner run. See the MarkerMiner manuscript (Chamala et al. 2014) for a more detailed discussion of default parameters.

![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/image_8.png)

**Figure 1.3.**: Default parameters are automatically displayed in MarkerMiner.

 

**4. Submit Jobs, Monitor Results, and Retrieve Output:**

Before submitting a MarkerMiner job, users have the option of providing a valid e-mail address that is used to notify the user upon start/completion of the job. Click on the "Submit Job" button to start your run (see Figure 1.3 above).

After a MarkerMiner job is submitted, users are redirected to a new page that displays the job status (e.g. "Job has been submitted successfully") and provides important links to help users monitor a MarkerMiner run (Figure 1.4). For example, users can monitor the current job status via the “browse/view” link or retrieve tar gzipped file output via the “download” link.

Users who did not provide an email address prior to submission should bookmark the status page so that you may return to it to check on the status of the job.

![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/image_9.png)

**Figure 1.4.** Job submission status page containing download links

Users who provided an email address will be sent a job submission notification email (Figure 1.5). Users should save this e-mail, since it includes links that can be used to retrieve their results. Once the job is complete, these users will also receive a final notification email, but it will NOT include a data retrieval link.

![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/image_10.png)

**Figure 1.5.** Job submission e-mail notification from MarkerMiner.

 

![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/image_11.png)

**Figure 1.6.** Job completion e-mail notification from MarkerMiner.

 

**5. Retrieving Results and Output:**

Click on the output retrieval link provided on either the MarkerMiner job submission status page (Figure 1.4) or in the MarkerMiner e-mail notification (Figure 1.5). The output will automatically download as a tar gzipped directory to a users computer (Figure 1.7).

![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/image_12.png)

**Figure 1.7.** MarkerMiner output downloads as a tar gzipped (.tar.gz) directory.

 
