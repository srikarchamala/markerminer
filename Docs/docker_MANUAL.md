![image alt text](../Media/MarkerMiner_logo_BW_DNA4_V1.jpeg)
** **

**IMPORTANT !!!!** - Use [Chrome](http://www.google.com/chrome/) Web Browser to view this page. As some of the other web browsers were unable to navigate through web-links in this document.

## Installing Docker and Running MarkerMiner

The MarkerMiner pipeline and its required dependencies have been encapsulated into a Docker **image** and is made available via the Docker Hub registry, allowing users to easily spin up an **containers** based on the image for their use.

For a simple introduction to Docker, please visit: https://www.docker.com/whatisdocker/

A [Docker image](https://docs.docker.com/terms/image/) is a lightweight Virtual Machine with only the filesystem and dependencies for the app installed, without any system level configuration.

A [Docker container](https://docs.docker.com/terms/container/) is an instance of a Docker image, which allows execution of processes within the container. Docker allows spinning up several containers from an image, where each container is completely isolated from the other.

Please follow the step-by-step instructions outlined below to get started with Docker.

**1. Visit the Docker website at [http://www.docker.com](http://www.docker.com). Click on the "Install & Docs" menu tab.**

![image alt text](../Media/Docker_image1.png)

**Figure 1** Docker home page

**2. From the "Installation" menu tab, choose your operating system, follow the installation instructions and install Docker.**

![image alt text](../Media/Docker_image2.png)

**Figure 2** "Installation" menu for various operating systems

**4. For users on the Mac OS X operating system, scroll down to part where it says "Install Boot2Docker" and following the steps to initialize the Docker environment.**

* 4.1. Go to the [boot2docker/osx-installer](https://github.com/boot2docker/osx-installer/releases/latest) release page.

* 4.2. Download Boot2Docker by clicking Boot2Docker-x.x.x.pkg in the "Downloads" section.

* 4.3. Install Boot2Docker by double-clicking the package. The installer places Boot2Docker in your "Applications" folder. The installation places the docker and boot2docker binaries in your /usr/local/bin directory.

**5. Launch boot2docker from your Applications folder**

When you launch the "Boot2Docker" application from your "Applications" folder, the application:

* opens a terminal window (see below image)
* creates a $HOME/.boot2docker directory
* creates a VirtualBox ISO and certs
* starts a VirtualBox VM running the docker daemon

![image alt text](../Media/Docker_image4.mac.png)

**Figure 3** Command-line terminal of configured with the Docker environment on Mac OS X

**6. The rest of this user manual guides through the nuances of running the MarkerMiner pipeline using a pre-configured Docker image. Please note, the majority of commands specified below are agnostic of the users' operating system.**

Issuing the `docker` command shows the standard usage and available list of commands. Below, we ONLY list out some of the most common commands we would need to use, within the scope of this document.

```
$ docker
Usage: docker [OPTIONS] COMMAND [arg...]

A self-sufficient runtime for linux containers.

Commands:
    exec      Run a command in a running container
    images    List images
    kill      Kill a running container
    logs      Fetch the logs of a container
    ps        List containers
    pull      Pull an image or a repository from a Docker registry server
    rename    Rename an existing container
    restart   Restart a running container
    rm        Remove one or more containers
    rmi       Remove one or more images
    run       Run a command in a new container
    start     Start a stopped container
    stop      Stop a running container
    top       Lookup the running processes of a container
```

* **6.1. Checking for existing images**

Command:

```
$ docker images

```
Output:

```
REPOSITORY          TAG                 IMAGE ID            CREATED      VIRTUAL SIZE

```

There are no images listed because we haven't created or downloaded one.

* **6.2. Download the pre-built MarkerMiner image from Docker Hub**

The [Docker hub](https://hub.docker.com) website is a registry of publicly and privately available Docker repositories. The MarkerMiner pipeline and all of its dependencies have been `dockerized` and made available from Docker hub, at the following URL: https://registry.hub.docker.com/u/vivekkrish/markerminer/.

Using the following command, a Docker image of MarkerMiner will be downloaded to the local filesystem. Time taken for the download is contingent on the network speed and available bandwidth.

Command:

```
$ docker pull vivekkrish/markerminer
```

Output:

```
Pulling repository vivekkrish/markerminer
02b10f20762d: Pulling dependent layers
f1b10cd84249: Download complete
8b44529354f3: Download complete
d1477a58d795: Download complete
7d688520dd35: Downloading [======================>                            ] 26.49 MB/58.17 MB...
...
...
Status: Downloaded newer image for vivekkrish/markerminer:latest
```

* **6.3. Checking for locally available Docker images**

Command:

```
$ docker images
```

Output:

```
REPOSITORY               TAG            IMAGE ID            CREATED       VIRTUAL SIZE
vivekkrish/markerminer   latest         02b10f20762d        43 hours ago  5.885 GB
```

The REPOSITORY name (eg. vivekkrish/markerminer) and IMAGE ID (eg. 02b10f20762d) can be used interchangeably for interacting with the images in the following ways: updating or deleting images, or initializing containers based on available images.

* **6.4. Identify available CPU resources**

Before initializing a Docker container based on the MarkerMiner image, we need to check the available number of CPUs that can be allocated to a particular container. This can be accomplished in multiple ways: (1) referring to your OS specific system information panel, (2) by viewing the following Docker help documentation.

Command:

```
$ docker run --help

```

Output:

```
Usage: docker run [OPTIONS] IMAGE [COMMAND] [ARG...]

Run a command in a new container

  -a, --attach=[]            Attach to STDIN, STDOUT or STDERR
  --add-host=[]              Add a custom host-to-IP mapping (host:ip)
  -c, --cpu-shares=0         CPU shares (relative weight)
  --cap-add=[]               Add Linux capabilities
  --cap-drop=[]              Drop Linux capabilities
  --cgroup-parent=           Optional parent cgroup for the container
  --cidfile=                 Write the container ID to the file
  --cpuset-cpus=             CPUs in which to allow execution (0-3, 0,1)
  ...
  ...
```

The help documentation seen above automatically identifies the total number of CPUs on your system and indicates the value in the command line param:
`"--cpuset-cpus=             CPUs in which to allow execution (0-3, 0,1)`.

In this case it lists the available CPU sets as "0-3" i.e. there are four CPUs available for use.

* **6.5. Creating a container from the MarkerMiner image**

Command:

```
$ docker run -d -t -c 3 -m 4g --name markerminer -p 5000:5000 -i -t vivekkrish/markerminer
```
Output:

```
d8b20cc23b0124288e170f5bd41784661374d7d8d0f6064350ce2d64b88a9de1
```

On successful instantiation of a container, the output generated will be the unique container ID.
To get list of all active containers, issue the command below:

Command:

```
$ docker ps
```

Output:

```
CONTAINER ID        IMAGE                COMMAND                CREATED             STATUS              PORTS                    NAMES
d8b20cc23b01        markerminer:latest   "/bin/sh -c 'python    30 minutes ago      Up 30 minutes       0.0.0.0:5000->5000/tcp   markerminer
```

"CONTAINER ID" is the unique hash identifying the container. Either the "CONTAINER ID" or "NAME" can be used interchangeably to refer to a particular container in further steps.

* **6.6. Running MarkerMiner**

Using the above instantiated container, the MarkerMiner pipeline can be run on any dataset of interest with the help of the supporting job submission web interface.

For users on the Linux platforms such as Ubuntu, CentOS, etc., the web interface can be accessed at the following URL:  
[http://0.0.0.0:5000](http://0.0.0.0:5000)

For users on the Mac OS X or Windows operating systems, find out the IP address of the Docker VM by issuing the following command:

Command:

```
$ boot2docker ip
```

Output:

```
192.168.59.103
```

Visit the following URL in your web browser (adjust IP address in the URL based on the output of the above command):  
[http://192.168.59.103:5000](http://192.168.59.103:5000)

For the rest of this tutorial, please refer to "**SECTION 2: Instructions for using the MarkerMiner WebApp**" of the [MarkerMiner iPlant User Manual](iPlant_MANUAL.md) for detailed instructions on how to submit jobs via the web interface.


**7. Stopping Docker containers and cleaning up**

Once you are done with a particular Docker container, you can stop and or completely remove unwanted containers by issuing the following commands:

Command:

```
$ docker stop markerminer
$ docker rm markerminer
```

Both the `stop` and `rm` command(s) can take as parameter, the name of the container or the unique CONTAINER ID (refer to the output of the `docker ps` command).
