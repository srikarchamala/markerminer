![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/MarkerMiner_logo_BW_DNA4_V1.jpeg)
** **

**IMPORTANT !!!!** - Use [Chrome](http://www.google.com/chrome/) Web Browser to view this page. As some of the other web browsers were unable to to navigate thourgh web-links.

Running MarkerMiner Locally
---
####SECTION 1: Installing MarkerMiner Dependencies 
* **Installing MarkerMiner Dependencies** - [Click here](https://bitbucket.org/srikarchamala/markerminer/src/HEAD/INSTALL.md?at=master).

####SECTION 2: Quick Check for  Dependencies Installation

Below are the list of required dependencies,

* Python (>= 2.7)
	* Modules: biopython, mandrill
* NCBI BLAST+ (>= 2.2.x)
* MAFFT (>= 7.x)

**1. Python**

* **1.1. Python program**

Command:

```
bash-3.2$ python -V
```
Output:

```
Python 2.7.5
```
Python version should be >= 2.7

* **1.2. Python biopython module**

Command:

```
bash-3.2$ python -c "import Bio"
```
Output:

```

```
If biopython module is installed then there will be no output, if not there will be an error saying "ImportError: No module named Bio"

* **1.2. Python mandrill module**

Command:

```
bash-3.2$ python -c "import mandrill"
```
Output:

```

```
If mandrill module is installed then there will be no output, if not there will be an error saying "ImportError: No module named mandrill"

**2. Mafft Software**
Command:

```
bash-3.2$ mafft
```
Output:

```
MAFFT v7.127b (2013/10/29)

        Copyright (c) 2013 Kazutaka Katoh
        MBE 30:772-780 (2013), NAR 30:3059-3066 (2002)
        http://mafft.cbrc.jp/alignment/software/
```

If Mafft Software is installed then there will be output as seen above. MAFFT veriosn >= 7.x is required.


**2. NCBI BLAST+ Software**
Command:

```
bash-3.2$ blastn -version
```
Output:

```
blastn: 2.2.28+
Package: blast 2.2.28, build Mar 12 2013 16:52:31
```

If NCBI BLAST+ Software is installed then there will be output as seen above. MAFFT veriosn >= 2.2.x is required.


####SECTION 3: Commandline usage
**1. Mandatory Input Option - A path for directory or folder with assembled transcripts fasta files (for -transcriptFilesDir option)**

MarkerMiner will accept a path to the directory or folder with assembled transcriptome data files in FASTA format. So all transcriptome data files that needs to analysed need to be place in this folder. Users can process a single FASTA file or multiple FASTA files. However, all file names must use the following naming convention: file names must start with a **four-letter species code followed by a hyphen** (e.g. "DAT1-", "DAT2-", "DAT3-", etc.; illustrated below box). Also, file names should only be ending in either **".fa"** or **".fasta"** or **".fsa"**.


Example of content inside the directory or folder containing assembled input  transcriptome data files in FASTA format e.g., "/home/schamala/MarkerMiner_TestRun/Sample_input":

```
DAT1-sample.fasta
DAT2-sample.fasta
DAT3-sample.fasta
DAT4-sample.fasta
```

**2. Set up the PYTHONPATH variable to specify location of MarkerMiner**

If you cloned the MarkerMiner repository to the directory `~/code/markerminer`, set up your SHELL environment as specified below:

If you use the bash shell, run the following command:

```
$ export PYTHONPATH=~/code/markerminer:$PYTHONPATH
```

If you use the c shell, run the following command instead:

```
$ setenv PYTHONPATH ~/code/markerminer:$PYTHONPATH
```

**3. Example to illustarate command-line usage of MarkerMiner**


```
python ~/code/markerminer/markerMiner.py \
-transcriptFilesDir ~/data/MarkerMiner_TestRun/Sample_input \
-singleCopyReference Athaliana \
-cpus 16 \
-minTranscriptLen 900 \
-outputDirPath ~/data/MarkerMiner_TestRun/sample_output \
-overwrite
```

**NOTE**: For "-transcriptFilesDir" and "-outputDirPath" provide absolute or complete path for the folders (not relative paths)

Once the program is done running successfully you should be seeing compressed output file, in this scenario we would see "sample_output.tar.gz". Description of the MarkerMiner ouput can be found in **[MarkerMiner Overview Page](https://bitbucket.org/srikarchamala/markerminer)** .

**4. All MarkerMiner Options**

```
$ python ~/code/markerminer/markerMiner.py -h

usage: markerMiner.py [-h] [-transcriptFilesDir TRANSCRIPTFILESDIR]
                      [-singleCopyReference {Alyrata,Athaliana,Bdistachyon,Cpapaya,Fvesca,Gmax,Mdomestica,Mesculenta,Mtruncatula,Osativa,Ptrichocarpa,Rcommunis,Sbicolor,Tcacao,Vvinifera,Zmays}]
                      [-minTranscriptLen MINTRANSCRIPTLEN]
                      [-minProteinCoverage MINPROTEINCOVERAGE]
                      [-minTranscriptCoverage MINTRANSCRIPTCOVERAGE]
                      [-minSimilarity MINSIMILARITY] [-cpus CPUS]
                      -outputDirPath OUTPUTDIRPATH [-email EMAIL]
                      [-sampleData] [-debug] [-overwrite]

MarkerMiner: Effectively discover single copy nuclear loci in flowering
plants, from user-provided angiosperm transcriptomes. Your are using MarkerMiner version 1.1

optional arguments:
  -h, --help            show this help message and exit
  -transcriptFilesDir TRANSCRIPTFILESDIR
                        Absolute or complete path of the transcript files
                        fasta directory. Only files ending with '.fa' or
                        '.fasta' will be accepted. Also, all file names must
                        use the following naming convention: file names must
                        start with a four-letter species code followed by a
                        hyphen (e.g. 'DAT1-', 'DAT2-', 'DAT3-', etc. (default:
                        None)
  -singleCopyReference {Alyrata,Athaliana,Bdistachyon,Cpapaya,Fvesca,Gmax,Mdomestica,Mesculenta,Mtruncatula,Osativa,Ptrichocarpa,Rcommunis,Sbicolor,Tcacao,Vvinifera,Zmays}
                        Choose from the available single copy reference
                        datasets (default: Athaliana)
  -minTranscriptLen MINTRANSCRIPTLEN
                        min transcript length (default: 900)
  -minProteinCoverage MINPROTEINCOVERAGE
                        min percent of protein length aligned (default: 80)
  -minTranscriptCoverage MINTRANSCRIPTCOVERAGE
                        min percent of transcript length aligned (default: 70)
  -minSimilarity MINSIMILARITY
                        min similarity percent with which seqeunces are
                        aligned (default: 70)
  -cpus CPUS            cpus to be used (default: 4)
  -outputDirPath OUTPUTDIRPATH
                        Absolute or complete path of output directory
                        (default: .)
  -email EMAIL          Specify email address to be notified on job completion
                        (default: None)
  -sampleData           run pipeline on sample datasets (default: False)
  -debug                turn on debug mode (default: False)
  -overwrite            overwrite results if output folder exists (default:
                        False)
```

**NOTE**: For "-transcriptFilesDir" and "-outputDirPath", please provide abosulte/complete paths for the folders (not relative paths).

** **

