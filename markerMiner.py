#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os
import os.path as op
import sys
import argparse
from collections import Counter
import glob
import shutil
from tempfile import mkstemp
from time import time, localtime, strftime
import multiprocessing as mp

from Bio import Seq, SeqIO

from blast import runBlast
from msa import multiple_seq_alignment
from helpers import outdir, start_process, prepare_directories, \
    renameFastaHeaders, touch, partition_jobs, getFastaFilePaths, \
    setup_custom_logger
from sendmail import EmailClient

from _version import __version__

SCRIPT_PATH = op.dirname(op.abspath(__file__))
SAMPLE_DATA_PATH = op.join(SCRIPT_PATH, "Sample_Data", "Sample_input")
RESOURCES_PATH = op.join(SCRIPT_PATH, "Resources")
AVAILABLE_REFERENCES = [d for d in os.listdir("{0}/".format(RESOURCES_PATH)) \
               if op.isdir(op.join(RESOURCES_PATH, d))]

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=
            "MarkerMiner: Effectively discover single copy nuclear loci in flowering plants, from user-provided angiosperm transcriptomes.\n"+\
            "Your are using MarkerMiner version "+__version__ ,
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-transcriptFilesDir", action="store", type=str,
           required=False, help="Absolute or complete path of the transcript files fasta directory. Only files ending with '.fa' or '.fasta' will be accepted. "+\
        "Also, all file names must use the following naming convention: file names must start with a four-letter species code followed by a hyphen (e.g. 'DAT1-', 'DAT2-', 'DAT3-', etc.")
    parser.add_argument("-singleCopyReference", action="store",
           type=str, default="Athaliana", choices=AVAILABLE_REFERENCES,
           help="Choose from the available single copy reference datasets")

    parser.add_argument("-minTranscriptLen", action="store", type=int,
           default=900, help="min transcript length")
    parser.add_argument("-minProteinCoverage", action="store", type=float,
           default=80, help="min percent of protein length aligned")
    parser.add_argument("-minTranscriptCoverage", action="store", type=float,
           default=70, help="min percent of transcript length aligned")
    parser.add_argument("-minSimilarity", action="store", type=float,
           default=70, help="min similarity percent with which " +
           "seqeunces are aligned")
    parser.add_argument("-cpus", action="store", type=int,
           default=4, help="cpus to be used")
    parser.add_argument("-outputDirPath", action="store", type=str,
           default=".", required=True, help="Absolute or complete path of output directory")

    parser.add_argument("-email", action="store", type=str,
           required=False, help="Specify email address to be notified on job completion")
    parser.add_argument("-sampleData", action="store_true",
           required=False, help="run pipeline on sample datasets")
    parser.add_argument("-debug", default=False, action="store_true",
           required=False, help="turn on debug mode")
    parser.add_argument("-overwrite", action="store_true",
           required=False, help="overwrite results if output folder exists")

    args = parser.parse_args()

    transcriptFilesDir = args.transcriptFilesDir
    singleCopyReference = args.singleCopyReference
    singleCopyReferencePrefix = singleCopyReference[:3].lower()
    minTranscriptLen = args.minTranscriptLen
    minProteinCoverage = args.minProteinCoverage
    minTranscriptCoverage = args.minTranscriptCoverage
    minSimilarity = args.minSimilarity
    cpus = args.cpus
    outputDirPath = args.outputDirPath
    email = args.email

    sampleData = args.sampleData
    debug = args.debug
    overwrite = args.overwrite

    transcriptFilePaths = op.join(outputDirPath, "input_transcriptomes.txt")
    outputFile = op.join(outputDirPath, "single_copy_genes.txt")
    logfileName = op.join(outputDirPath, "markerminer_run_logfile.txt")
    secondaryTranscriptsFile = op.join(outputDirPath, "single_copy_genes.secondaryTranscripts.txt")

    PROTEIN_FILE = op.join(RESOURCES_PATH, singleCopyReference, "proteome.{0}.tfa".format(singleCopyReference[:3].lower()))
    PROTEIN_BLAST_DB = op.join(RESOURCES_PATH, singleCopyReference, "proteome.{0}.tfa_db".format(singleCopyReference[:3].lower()))
    CDNA_INTRON_FILE = op.join(RESOURCES_PATH, singleCopyReference, "{0}_CDS_IntronMasked.fa".format(singleCopyReference[:3].lower()))
    SINGLE_COPY_IDS_FILE = op.join(RESOURCES_PATH, singleCopyReference, "singleCopy_{0}.txt".format(singleCopyReference[:2].upper()))

    prepare_directories(outputDirPath, overwrite=overwrite)

    logger = setup_custom_logger('markerminer', filename=logfileName)
    logger.info("Running MarkerMiner version "+__version__+"\n")

    cDNA_intron_fasta_dict = dict()
    for fastaRec in SeqIO.parse(CDNA_INTRON_FILE, "fasta"):
        cDNA_intron_fasta_dict[fastaRec.id] = str(fastaRec.seq)

    singleCopyIDs_dict = dict()
    annotation_dict = dict()

    if not transcriptFilesDir and not sampleData:
        sys.exit("Either -transcriptFilesDir or -sampleData option is required.\n")

    if sampleData:
        transcriptFilesDir = SAMPLE_DATA_PATH

    getFastaFilePaths(transcriptFilesDir, transcriptFilePaths)

    in_file = open(SINGLE_COPY_IDS_FILE, 'rU')
    for line in in_file:
        line = line.rstrip("\n")
        splitLine = line.split("\t")
        singleCopyIDs_dict[splitLine[0]] = splitLine[1]
        annotation_dict[splitLine[0]] = splitLine[2]
    in_file.close()

    manager = mp.Manager()
    finalDict = manager.dict()

    speciesDict = dict()
    fastaRecDict = dict()

    N = sum(1 for line in open(transcriptFilePaths))
    bprocs, bcpus = partition_jobs(N, cpus)

    # create pool based on desired number of parallel BLAST processes
    blast_pool = mp.Pool(processes=bprocs, initializer=start_process)

    in_file = open(transcriptFilePaths, 'rU')
    for line in in_file:
        line = line.rstrip("\n")
        filepath, filename = op.split(line)

        speciesCode = filename.split("-")[0]
        speciesDict[speciesCode] = " "
        for id in singleCopyIDs_dict.keys():
            finalDict[(id, speciesCode)] = (None, None, [])

        outfile = op.join(outputDirPath, outdir['LENGTH_FILTERED_FASTA_DIR'], \
                "MinLen{0}_{1}".format(minTranscriptLen, filename))

        blastPath = op.join(outputDirPath, outdir['BLAST_DIR'])
        blastDb_name = op.join(blastPath, \
            "MinLen{0}_{1}_db".format(minTranscriptLen, filename))
        blastxout = op.join(blastPath, \
            "MinLen{0}_{1}_BlastxOut.txt".format(minTranscriptLen, filename))
        tblastnout = op.join(blastPath, \
            "MinLen{0}_{1}_TblastnOut.txt".format(minTranscriptLen, filename))

        renameFastaHeaders(line, outfile, fastaRecDict, \
                prefix=speciesCode + "_", suffix="", minLen=minTranscriptLen)

        blast_pool.apply_async(runBlast, ( \
                (outfile, blastDb_name, blastPath, debug), \
                ("blastx", outfile, PROTEIN_BLAST_DB, bcpus, blastxout, debug), \
                ("tblastn", PROTEIN_FILE, blastDb_name, bcpus, tblastnout, debug), \
                minTranscriptCoverage, minProteinCoverage, minSimilarity, \
                speciesCode, singleCopyIDs_dict, finalDict))

    blast_pool.close()
    blast_pool.join()

    ## for the MSA steps (mafft, muscle), allot more parallel processes and less cpu per process
    mprocs, mcpus = cpus, 1

    # create pool based on desired number of parallel MSA processes
    msa_pool = mp.Pool(processes=mprocs, initializer=start_process)
    msa_pool_args = []

    out_fh = open(outputFile, 'w')
    out_fh1 = open(secondaryTranscriptsFile, 'w')
    headers = ["geneId", "singleCopyStatus", "geneFunction", "numOfOrthologs"]
    headers.extend(sorted(speciesDict.keys()))
    out_fh.write("{0}\n".format("\t".join(headers)))
    for geneId in sorted(singleCopyIDs_dict.keys()):
        outputList, outputFastaList = [], []
        outputList.extend([geneId, singleCopyIDs_dict[geneId], \
            annotation_dict[geneId], ""])
        for speciesCode in sorted(speciesDict.keys()):
            transcript_id, strand, secondaryTranscriptList = finalDict[(geneId, speciesCode)]
            if transcript_id is not None:
                record = fastaRecDict[transcript_id]
                if strand == "-":
                    record = record.reverse_complement(id=transcript_id, \
                            description ="")
                outputFastaList.append(record)
                outputList.append("_".join(str(x) for x in (transcript_id, strand)))

                if len(secondaryTranscriptList) > 0:
                    secondaryTranscriptLine = ";".join("{0}_{1}".format(t, s) \
                            for (t, s) in secondaryTranscriptList)

                    out_fh1.write("\t".join(str(x) for x in \
                        [geneId,transcript_id,secondaryTranscriptLine]) + "\n")
            else:
                outputList.append("NA")

        ct = Counter(outputList)
        if ct.has_key("NA"):
            del ct["NA"]

        # at least one species have orthologus copy to Arabidopsis
        if len(ct) < 5:
            continue
        # assigning no. of orthologs
        outputList[3] = str(len(ct)-4)

        out_fh.write("{0}\n".format("\t".join(outputList)))

        temp_fh, cDNA_intron_fasta_file = mkstemp(dir=outputDirPath, \
            prefix="{0}_".format(geneId), suffix=".fasta")
        os.write(temp_fh, ">{0}\n{1}\n".format(geneId, cDNA_intron_fasta_dict[geneId]))
        os.close(temp_fh)

        msa_pool.apply_async(multiple_seq_alignment, (outputFastaList, \
                    cDNA_intron_fasta_file, mcpus, outputDirPath, geneId, debug))

    msa_pool.close()
    msa_pool.join()

    in_file.close()
    out_fh.close()
    out_fh1.close()
    logger.info("Done\n")

    root_dir, base_dir = op.dirname(op.abspath(outputDirPath)), op.basename(outputDirPath)
    shutil.make_archive(outputDirPath, format="gztar", root_dir=root_dir, base_dir=base_dir)
    cmd = "rm -r {0}".format(outputDirPath)
    logger.info("Cleaning up output directory path: {0}\n".format(cmd))
    os.system(cmd)

    if email:
        email_handler = EmailClient()
        email_handler.send_email(email)
