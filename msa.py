#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os
import os.path as op

from Bio import Seq, SeqIO, AlignIO

from helpers import outdir

import logging
logger = logging.getLogger("markerminer")


def multiple_seq_alignment(nucSeqsObjList, cDNA_intron_fasta_file, cpus, \
    outputDirPath, outputFilePrefix, debug):

    nucClusterOutputFile = op.join(outputDirPath, outdir['NUC_FASTA_DIR'], \
            "{0}.nuc.fna".format(outputFilePrefix))
    mafftNucAlignmentOutputFile = op.join(outputDirPath, outdir['MAFFT_NUC_ALIGN_FASTA_DIR'], \
            "{0}.mafft.nuc.align.fna".format(outputFilePrefix))
    mafftNucPhylipAlignmentOutputFile = op.join(outputDirPath, outdir['MAFFT_NUC_ALIGN_PHY_DIR'], \
            "{0}.mafft.nuc.align.phy".format(outputFilePrefix))
    mafftAddRefNucAlignmentOutputFile = op.join(outputDirPath, outdir['MAFFT_ADD_REF_ALIGN_FASTA_DIR'], \
            "{0}.mafft.added.reference.align.fna".format(outputFilePrefix))

    logger.info("Started mafft: {0}\n".format(outputFilePrefix))
    if debug:
        touch(nucClusterOutputFile)
        touch(mafftNucAlignmentOutputFile)
    else:
        nucClusterOutputFile_handle = open(nucClusterOutputFile, 'w')
        SeqIO.write(nucSeqsObjList, nucClusterOutputFile_handle, "fasta")
        nucClusterOutputFile_handle.close()

        # mafft command produces empty output if there is only one input sequence
        # in such a case, just execute the cat command
        mafft_cmd = "mafft --quiet --auto --thread {0}".format(str(cpus)) \
            if len(nucSeqsObjList) > 1 else "cat"
        mafft_cmd += " {0} > {1}".format(nucClusterOutputFile, mafftNucAlignmentOutputFile)

        os.system(mafft_cmd)
    logger.info("Finished mafft: {0}\n".format(mafftNucAlignmentOutputFile))

    logger.info("Started mafft to phylip conversion: {0}\n".format(outputFilePrefix))
    if debug:
        touch(mafftNucPhylipAlignmentOutputFile)
    else:
        nucAlign_input_handle = open(mafftNucAlignmentOutputFile, 'rU')
        phyAlign_output_handle = open(mafftNucPhylipAlignmentOutputFile, 'w')
        alignments = AlignIO.parse(nucAlign_input_handle, "fasta")
        AlignIO.write(alignments, phyAlign_output_handle, "phylip-relaxed")
        nucAlign_input_handle.close()
        phyAlign_output_handle.close()
    logger.info("Finished mafft to phylip conversion: {0}\n".format(mafftNucPhylipAlignmentOutputFile))

    logger.info("Started mafft add reference to MSA: {0}\n".format(outputFilePrefix))
    if debug:
        touch(mafftAddRefNucAlignmentOutputFile)
    else:
        mafft_add_ref_cmd = "mafft --thread {3} --quiet --add {0} {1} >{2} ".format(cDNA_intron_fasta_file, \
                        mafftNucAlignmentOutputFile,
                        mafftAddRefNucAlignmentOutputFile, str(cpus))
        os.system(mafft_add_ref_cmd)
    logger.info("Finished mafft add reference to MSA: {0}\n".format(mafftAddRefNucAlignmentOutputFile))

    rm_cmd = "rm {0}".format(cDNA_intron_fasta_file)
    os.system(rm_cmd)
