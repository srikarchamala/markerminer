#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests

class EmailClient():
    def __init__(self, email_from='markerminer@gmail.com',
                 from_name='MarkerMiner Admin', api_key='0f50c712-2e60-44a4-9b5c-fe01cae252ff'):
        self.api_uri = 'https://api.elasticemail.com/v2'
        self.email_from = email_from
        self.from_name = from_name
        self.api_key = api_key

    def request(self, method, url, payload):
        payload['apikey'] = self.api_key
        if method == 'POST':
            result = requests.post(self.api_uri + url, params=payload)
        elif method == 'PUT':
            result = requests.put(self.api_uri + url, params=payload)
        elif method == 'GET':
            attach = ''
            for key in payload:
                attach = attach + key + '=' + payload[key] + '&'
            url = url + '?' + attach[:-1]
            result = requests.get(self.api_uri + url)
        response = result.json()
        if response['success'] is False:
            return response['error']
        return response['data']

    def _send_email(self, subject, email_from, from_name, email_to, body_html, body_text, is_txn):
        payload = {
            'subject': subject,
            'from': email_from,
            'fromName': from_name,
            'to': email_to,
            'bodyHtml': body_html,
            'bodyText': body_text,
            'isTransactional': is_txn
        }

        return self.request('POST', '/email/send', payload)

    def send_email(self, email_to):
        subject = 'MarkerMiner pipeline status: Completed'

        email_text_html = """
Your MarkerMiner job has completed successfully!<br />
Final results can be downloaded using link sent in the previously received submission email.<br />
        """

        email_text = """
Your MarkerMiner job has completed successfully!
Final results can be downloaded using link sent in the previously received submission email.
        """

        self._send_email(
            subject, self.email_from, self.from_name, email_to,
            email_text_html, email_text, True
        )
