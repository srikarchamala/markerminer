#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os.path as op
import logging
import subprocess as sp
import sys
from collections import OrderedDict,defaultdict

import logging
logger = logging.getLogger("markerminer")


blast_formatList = ["7", "qseqid", "sseqid", "pident", "length", "mismatch", \
        "gapopen", "qstart", "qend", "sstart", "send", "evalue", \
        "bitscore", "qlen", "slen", "positive", "ppos", \
        "qcovhsp", "qcovs", "frames"]


class BlastLineLocal(object):
    __slots__ = ("query", "subject", "pctid", "hitlen", "nmismatch", "ngaps", \
                 "qstart", "qstop", "sstart", "sstop", "evalue", "score", \
                 "qlen", "slen", "npositives", "pcpos", "qcovhsp", "qcovs", "orientation")

    def __init__(self, sline):
        args = sline.split("\t")
        self.query = args[0]
        self.subject = args[1]
        self.pctid = float(args[2])
        self.hitlen = int(args[3])
        self.nmismatch = int(args[4])
        self.ngaps = int(args[5])
        self.qstart = int(args[6])
        self.qstop = int(args[7])
        self.sstart = int(args[8])
        self.sstop = int(args[9])
        self.evalue = float(args[10])
        self.score = float(args[11])
        self.qlen = int(args[12])
        self.slen = int(args[13])
        self.npositives = int(args[14])
        self.pcpos = float(args[15])
        self.qcovhsp = float(args[16])
        self.qcovs = float(args[17])

        if self.sstart > self.sstop:
            self.sstart, self.sstop = self.sstop, self.sstart
            self.orientation = "-"
        else:
            self.orientation = "+"

    def __repr__(self):
        return "BlastLine('%s' to '%s', eval=%.3f, score=%.1f)" % \
                (self.query, self.subject, self.evalue, self.score)

    def __str__(self):
        args = [getattr(self, attr) for attr in BlastLineLocal.__slots__[:19]]
        if self.orientation == "-":
            args[8], args[9] = args[9], args[8]
        return "\t".join(str(x) for x in args)

    def is_self_hit(self):
            return self.query==self.subject


def _blastOutputToDict(filename,minQueryCoverage,minPercPositive):
    fp = open(filename, 'rU')
    default_d = defaultdict(list)
    ordered_d = OrderedDict()
    for row in fp:
        if row[0] == "#":
            continue

        bline = BlastLineLocal(row)
        if bline.query == bline.subject:
            continue

        if bline.qcovs < minQueryCoverage :
            continue

        if bline.pcpos < minPercPositive:
            continue

        if (bline.query, bline.subject) not in ordered_d:
            ordered_d[(bline.query, bline.subject)] = bline
        else:
            cur_score = ordered_d[(bline.query, bline.subject)].score
            if bline.score > cur_score:
                ordered_d[(bline.query, bline.subject)] = bline

    for key in ordered_d.keys():
        bline = ordered_d[key]
        default_d[bline.query].append((bline.subject,bline.orientation))

    fp.close()
    return default_d

def _buildBlastDb(args):
    nucFastafile, blastDb_name, outputPath, debug = args

    outfile = blastDb_name
    logfile = "{0}_blastDbLog.txt".format(blastDb_name)

    logger.info("Started makeblastdb: {0}\n".format(blastDb_name))
    if debug:
        touch(outfile)
        touch(logfile)
    else:
        cmd = ["makeblastdb", "-dbtype", "nucl", \
                "-in", nucFastafile, "-out", outfile, \
                "-logfile", logfile]
        proc = sp.Popen(cmd)
        proc.wait()
    logger.info("Finished makeblastdb: {0}\n".format(outfile))


def _runBlast(args):
    prog, queryFile, blastDb_name, cpus, outputFile, debug = args
    filepath, filename = op.split(queryFile)

    logger.info("Started {0}: {1} against {2}\n".format(prog, queryFile, blastDb_name))
    if debug:
        touch(outputFile)
    else:
        fh = open(outputFile, 'w')
        proc = sp.Popen([prog, "-db", blastDb_name, "-evalue", "0.01", \
                "-query", queryFile, "-num_threads", str(cpus), \
                "-outfmt", " ".join(blast_formatList)], \
                stdout=fh)

        proc.wait()
        fh.close()
    logger.info("Finished {0}: {1}\n".format(prog, outputFile))

    return outputFile


def runBlast(buildBlastDbArgs, blastxargs, tblastnargs, \
        minTranscriptCoverage, minProteinCoverage, minSimilarity, \
        speciesCode, singleCopyIDs_dict, finalDict):

    _buildBlastDb(buildBlastDbArgs)

    _runBlast(blastxargs)
    _runBlast(tblastnargs)

    blastx_dict = _blastOutputToDict(filename=blastxargs[4],\
            minQueryCoverage=minTranscriptCoverage,\
            minPercPositive=minSimilarity)

    tblastn_dict = _blastOutputToDict(filename=tblastnargs[4],\
            minQueryCoverage=minProteinCoverage,\
            minPercPositive=minSimilarity)

    resultDict = dict()
    for singleCopyGene in singleCopyIDs_dict:
        if tblastn_dict.has_key(singleCopyGene):
            topTranscriptID, orientation = tblastn_dict[singleCopyGene][0]
            topProteinID = ""
            if blastx_dict.has_key(topTranscriptID):
                topProteinID = blastx_dict[topTranscriptID][0][0]

            if singleCopyGene == topProteinID:
                secondaryTranscriptsList = tblastn_dict[singleCopyGene][1:]
                keptSecTransList = []
                for secondaryTranscript, secondaryOrientation in secondaryTranscriptsList:
                    if  blastx_dict.has_key(secondaryTranscript):
                        secondaryTranscriptProteinsList = blastx_dict[secondaryTranscript]
                        for temp_protein, temp_orient in secondaryTranscriptProteinsList:
                            if temp_protein == singleCopyGene and \
                                len(secondaryTranscriptProteinsList) == 1:
                                keptSecTransList.append((secondaryTranscript, secondaryOrientation))

                resultDict[(singleCopyGene, speciesCode)] = \
                    (topTranscriptID, orientation, keptSecTransList)

    finalDict.update(resultDict)
