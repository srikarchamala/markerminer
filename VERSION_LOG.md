![image alt text](https://bitbucket.org/srikarchamala/markerminer/raw/master/Media/MarkerMiner_logo_BW_DNA4_V1.jpeg)
** **

MarkerMiner Change Log 
---

### Version 1.2:
* **Output files are now compressed into a `.tar.gz` file instead of a `.zip` file**

### Version 1.1:
* **Replaced "single_gene_identification.py" with "markerMiner.py"** 
* **Replaced "-transcriptFilePaths" with "-transcriptFilesDir"** : Rather than providing a file with paths for the fasta transcriptome assembly files, now user just have to provide directory path where these files reside, and the MarkerMiner program will automatically detects these files with ".fasta" or ".fa" or ".fsa". These file names will be reported in MarkerMiner output folder in "input_transcriptomes.txt" file.
* **Removed "-outputFile" option** : Now Markerminer automatically outputs single copy nuclear loci in to "single_copy_genes.txt" file of output folder.
* **Removed "-logfileName" option** : Now Markerminer automatically outputs log report into in to "markerminer_run_logfile.txt" file of output folder.
* **Bug fix** : For transcriptome fasta headers ending with '.' (e.g., >scaffold-dfdjl_sp.) is causing MarkerMiner to abort and this has been fixed.
