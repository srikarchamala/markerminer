Installing MarkerMiner Dependencies
---

####SECTION 1: Dependencies


* Python (>= 2.7)
	* Modules: biopython
* NCBI BLAST+ (>= 2.2.x)
* MAFFT (>= 7.x)

** **

####SECTION 2: Installing Dependencies

Below are the detailed installation information on a CentOS 6.x system.


**2.1. Switch to and install as root user**

```
sudo su -
```
**2.2. Install development tools and some core packages**

```
yum -y update
yum groupinstall --setopt=group_package_types=optional -y 'Development Tools'
yum groupinstall -y 'Scientific Support'
yum install -y bzip2-devel httpd-devel.x86_64
```

**2.3. Install Python 2.7.9 and setuptools**

```
cd ~/ && mkdir Downloads && cd Downloads
wget https://www.python.org/ftp/python/2.7.9/Python-2.7.9.tgz
tar -zxf Python-2.7.9.tgz
cd Python-2.7.9
./configure --prefix=/usr/local --enable-shared LDFLAGS="-Wl,--rpath=/usr/local/lib"
make
make altinstall
cd /usr/local/bin && ln -sf python2.7 python
cd ~/Downloads
wget https://bootstrap.pypa.io/ez_setup.py -O - | /usr/local/bin/python
```

**2.4. Install Biopython and other required Python modules**

```
easy_install -f http://biopython.org/DIST/ biopython
pip install virtualenv
```

**2.5. Install NCBI BLAST+**

```
wget ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST//ncbi-blast-2.2.30+-x64-linux.tar.gz
tar -zxf ncbi-blast*.tar.gz
cd ncbi-blast-*
cp -pr bin/* /usr/local/bin/.
cd ../
```


**2.6. Install Mafft**

```
wget http://mafft.cbrc.jp/alignment/software/mafft-7.215-with-extensions-src.tgz
tar -zxf mafft-7.*-with-extensions-src.tgz
cd mafft-7.*-with-extensions/core/
make clean
make
make install
cd ../extensions
make clean
make
make install
cd ../../
```
** **
