#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os
import glob
import os.path as op
import sys
import multiprocessing as mp

from Bio import Seq, SeqIO

import logging
logger = logging.getLogger("markerminer")

outdir = {
  'LENGTH_FILTERED_FASTA_DIR' : 'LENGTH_FILTERED_FASTA',
	'BLAST_DIR' : 'BLAST',
	'NUC_FASTA_DIR' : 'NUC_FASTA',
	'MAFFT_NUC_ALIGN_PHY_DIR' : 'MAFFT_NUC_ALIGN_PHY',
	'MAFFT_NUC_ALIGN_FASTA_DIR' : 'MAFFT_NUC_ALIGN_FASTA',
	'MAFFT_ADD_REF_ALIGN_FASTA_DIR' : 'MAFFT_ADD_REF_ALIGN_FASTA',
}

def start_process():
    logger.info('[multiprocessing] Starting {0}'.format(mp.current_process().name))


def prepare_directories(outputDirPath, overwrite=False):
    if op.isdir(outputDirPath):
        if overwrite:
            cmd = "rm -r {0}".format(outputDirPath)
            os.system(cmd)
        else:
            sys.exit("Following Directory you provided already exist; " + \
                    "if you want to overwrite please include -overwrite option " + \
                    "or enter different directory path\n{0}\n".format(outputDirPath))

    outdirs = [op.join(outputDirPath, x) for x in outdir.values()]
    outdirs.insert(0, outputDirPath)
    for opdir in outdirs:
        cmd = "mkdir {0}".format(opdir)
        os.system(cmd)


def renameFastaHeaders(fastaFile, outPutFile, appendDict, \
        prefix="", suffix="", minLen=None):
    output_handle = open(outPutFile, 'w')
    for fastaRec in SeqIO.parse(fastaFile, "fasta"):
        fastaId = fastaRec.id
        fastaRec.description = ""
        newId = prefix + fastaId + suffix
        while newId.endswith('.'):
            newId = newId[:-1]

        fastaRec.id = newId
        if len(fastaRec.seq) >= minLen:
            SeqIO.write(fastaRec, output_handle, "fasta")
            appendDict[fastaRec.id] = fastaRec

    output_handle.close()


def touch(fname, times=None):
    with open(fname, 'a'):
        os.utime(fname, times)


def getFastaFilePaths(fastaFilesDir, fastaFilesPathOut):
    extensions = ('*.fa', '*.fasta', '*.fsa')
    fw = open(fastaFilesPathOut, 'w')
    for i in extensions:
        for file in glob.glob(op.join(fastaFilesDir, i )):
            fw.write("{0}\n".format(file))
    fw.close()


def partition_jobs(N, cpus):
    if N > cpus: N = cpus
    nprocs, ncpus = N, int(cpus / float(N))
    return nprocs, ncpus


def setup_custom_logger(name, filename=None):
    logging.basicConfig(filename=filename, filemode='w', \
            format="%(asctime)s :: %(levelname)s ::" + \
                   " %(module)s :: %(message)s", \
            datefmt="%a, %d %b %Y %H:%M:%S", \
            level=logging.DEBUG)

    logger = logging.getLogger(name)
    return logger
